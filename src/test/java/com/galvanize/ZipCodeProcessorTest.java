package com.galvanize;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;


public class ZipCodeProcessorTest {


    private Verifier verifier;
    private ZipCodeProcessor zipCodeProcessor;

    @BeforeEach
    public void setUp() {
        verifier = new Verifier();
        zipCodeProcessor = new ZipCodeProcessor(verifier);
    }

    // write your tests here
    @Test
    public void testThankYouMessage() {
        //arrange
        String expected = "Thank you!  Your package will arrive soon.";
        //act
        String actual = zipCodeProcessor.process("92058");
        //assert
        assertEquals(expected, actual);
    }

    @Test
    public void testInputTooLong() {
        //arrange
        String expected = "The zip code you entered was the wrong length.";
        //act
        String actual = zipCodeProcessor.process("920589");
        //assert
        assertEquals(expected, actual);
    }

    @Test
    public void testInputTooShort() {
        //arrange
        String expected = "The zip code you entered was the wrong length.";
        //act
        String actual = zipCodeProcessor.process("9205");
        //assert
        assertEquals(expected, actual);
    }

    @Test
    public void testNoServiceTest() {
        //arrange
        String expected = "We're sorry, but the zip code you entered is out of our range.";
        //act
        String actual = zipCodeProcessor.process("12345");
        //assert
        assertEquals(expected, actual);
    }

}