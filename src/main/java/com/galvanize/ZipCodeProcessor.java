package com.galvanize;


public class ZipCodeProcessor {

    // don't alter this code...
    private final Verifier verifier;

    public ZipCodeProcessor(Verifier verifier) {
        this.verifier = verifier;
    }

    // write your code below here...
    public String process(String zipCode) {
        StringBuilder message = new StringBuilder();
        try {
            verifier.verify(zipCode);
            message.append("Thank you!  Your package will arrive soon.");
        } catch (InvalidFormatException e) {
            message.append("The zip code you entered was the wrong length.");
        } catch (NoServiceException e) {
            message.append("We\'re sorry, but the zip code you entered is out of our range.");
        }
        return message.toString();
    }
}
